const express = require('express')
const app = express()
const port = 3000
const config = {
    host: 'mysql',
    user: 'root',
    password: 'root',
    database: 'desafio'
};
const mysql = require('mysql')
const connection = mysql.createConnection(config)

const sql = `INSERT INTO people(name) VALUES('Renan')`
connection.query(sql)

app.get('/', (req, res) => {
    var select = connection.query('select name from people', function(error, results, fields) {
        let exibir = '<h1>Full Cycle Rocks!</h1>'+
                     '<ul>'

        results.forEach(row => {
            exibir += '<li>'+row.name+'</li>'
        });

        exibir += '</ul>'
        res.send(exibir)
    })
})

app.listen(port, () => {
    console.log('Rodando na porta '+port)
})